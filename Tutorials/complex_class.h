class complex{

public:
	float real;
	float imag;
	complex(float r=0,float i=0){        //constructor
		real=r; imag=i;
	}
	//operator overloading example, can be used for other cases
	complex operator+(complex c);
	
    //used to directly show output
	friend ostream& operator<< (ostream& out,const complex& number);
};

complex complex::operator+(complex c)
{
	complex temp;
	temp.real=real+c.real;
	temp.imag=imag+c.imag;
	return temp;
}

ostream& operator<< (ostream& out,const complex& number)
{
	out <<"(("<<number.real<<")+("<<number.imag<<")i)";
	return out;
}
