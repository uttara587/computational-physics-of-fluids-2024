set term png font "arial,16"
set title "Plot from data file"
set xlabel "x"
set ylabel "erf(x)"
set xrange [0:3]
set yrange [0:1.1]
set output "erf_custom.png"
plot "./erf.dat" u 1:2 with linespoints notitle
